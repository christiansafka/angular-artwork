var AngularArtwork = angular.module('AngularArtwork', ['ngAnimate']);


AngularArtwork.controller('mainController', ['$scope', '$http',
	function($scope, $http) {

		$scope.nextDisabled = true;
		$scope.correct = 0;
		$scope.total = 0;

		$http.get("/init").then(function(data) {
			$http.get("/api").then(function(data) {
			//console.log(data) to get object in console and figure out json routes.
				$scope.img1date = data.data.img1date;
				$scope.img2date = data.data.img2date;
				$scope.img1url = data.data.img1url;
				$scope.img2url = data.data.img2url;
			});
		});

		$scope.guess = function(selection) {

			$scope.total += 1;
		 	$scope.disabled = true;

		 	if(selection === 1)
		 	{
		 		if($scope.img1date <= $scope.img2date)
		 		{
		 			$scope.win = 1;
		 			$scope.correct += 1;
		 		}
		 		else 
		 		{ 
		 			$scope.win = 2;
		 		}
		 	}
		 	else
		 	{
		 		if($scope.img2date <= $scope.img1date)
		 		{
		 			$scope.win = 2;
		 			$scope.correct += 1;
		 		}
		 		else 
		 		{ 
		 			$scope.win = 1;
		 		}
		 	}

		 	$scope.nextDisabled = false;
		 	$scope.img1dateDisplay = $scope.img1date;
		 	$scope.img2dateDisplay = $scope.img2date;
	    };


	    $scope.next = function() {

	    	$scope.imgSwitch = true;
	    	$scope.nextDisabled = true;
	    	$scope.win = 0;
	    	$scope.img1dateDisplay = "";
	    	$scope.img2dateDisplay = "";

	    	$http.get('/api').success(function(data) {
					
					
					$scope.img1date = data.img1date;
					$scope.img2date = data.img2date;
					$scope.img1url = data.img1url;
					$scope.img2url = data.img2url;
					
					$scope.disabled = false;
					$scope.imgSwitch = false;
					
					
					

			});
					
	  	}
}]);
