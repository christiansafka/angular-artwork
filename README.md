# README #


### Angular Artwork ###

A simple artwork guessing game, where the user guesses which piece of artwork is the oldest.

This project uses NodeJS and Express for back-end, and AngularJS for front-end.

http://rijksmuseum.github.io/  public API is used for artwork images and data.


### Set Up ###

Requires NodeJS installed.

Change ' var APIKEY = "" ' in routes.js and input your API KEY from http://rijksmuseum.github.io/


### To Do ###

* Asynchronous requests to API
* Fix UI animation
* bug: API stops working randomly