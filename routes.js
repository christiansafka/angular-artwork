var request = require('request');

module.exports = function(app) {

	var artwork = {};
	var APIKEY = process.env.APIKEY;

	//get 100 pieces of art to work with
	app.get('/init', function(req, res) {  
		request('http://www.rijksmuseum.nl/api/en/collection?key=' + APIKEY + '&imgonly=true&ps=100&format=json', function (error, response, body) {
			  if (!error && response.statusCode == 200) {
			  	 	artwork = JSON.parse(body);
			  	 	res.send("done");
			  }
		});
	});

    app.get('/api', function(req, res) {

    	var json = { 'img1date': 'null', 'img1url': 'null', 'img2date': 'null', 'img2url': 'null'};
		var rNum = Math.floor((Math.random() * 100) + 1);
		var rNum2 = Math.floor((Math.random() * 100) + 1);

		var img1 = artwork.artObjects[rNum].objectNumber;
		var img2 = artwork.artObjects[rNum2].objectNumber;

		//get specific info of artwork
		request('https://www.rijksmuseum.nl/api/en/collection/' + img1 + '?key=' + APIKEY + '&format=json', function (error, response, body) {	
			parsedBody = JSON.parse(body);
			json.img1date = parsedBody.artObject.dating.year;
			json.img1url = parsedBody.artObject.webImage.url;

			request('https://www.rijksmuseum.nl/api/en/collection/' + img2 + '?key=' + APIKEY + '&format=json', function (error, response, body) {
				parsedBody = JSON.parse(body);
				json.img2date = parsedBody.artObject.dating.year;
				json.img2url = parsedBody.artObject.webImage.url;

				res.json(json);
			});
		});
	});

}