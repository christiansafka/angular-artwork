var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var methodOverride = require('method-override');
var request = require('request');

var app = express();


app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());                                     // parse application/json
app.use(methodOverride());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    next();
    });

require('./routes')(app);



app.get('/', function(req, res) {
        res.render('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

	/*request({
		url: 'http://www.rijksmuseum.nl/api/en/collection?key=t9EO4pgW&imgonly=true&p=2&ps=2&format=json',
		method: 'GET',
		function(error, response, body) {
			if(error)
			{
				console.log(error);
				res.status(500).send({ error: "failed to send"});
			}
			else
			{
				console.log("sending response body");
				res.status(200).send({body});
			}
		}
	});
	*/

app.listen(process.env.PORT || 8080, function() {
	console.log('ready');
});
